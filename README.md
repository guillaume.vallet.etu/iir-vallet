# Projet IIR - _Problématique_ 

Comment garder l'attention des utilisateurs en RV ?

## Étudiant

Vallet Guillaume : guillaume.vallet.etu@univ-lille.fr

## Article de référence

- **Citation :**
Lasse T. Nielsen, Matias B. Møller, Sune D. Hartmeyer, Troels C. M. Ljung∗
, Niels C. Nilsson, Rolf Nordahl, and Stefania Serafin†
Aalborg University Copenhagen

Missing The Point: An Exploration of How to Guide Users’ Attention
During Cinematic Virtual Reality
- **Conférence / Revue :** 
VRST '16: Proceedings of the 22nd ACM Conference on Virtual Reality Software and Technology
- **Classification :** A
- **Nombre de citations :** 80

## Résumé de l'article de référence


- **Problématique  :**
Comment garder l'attention de l'utilisateur dans un film en RV
- **Question de recherche :**
Garder l'attention de l'utilisateur sur les éléments cruciaux d'un film sans lui enlever son libre arbitre en RV
- **Démarche adoptée :**
Comparaison de deux approche différentes : 
	- Rotation forcée de la tête (Le corp est toujours orienté en face de la scène) -> Perte de présence et d'immersion attendu (Approche non-diégétique implicite)
	- Utilisateur libre, une petite luciole guide les yeux en se posant ou se dirigeant vers les points d'intérêt -> Moins efficace pour guider mais garde la sensation de présence et d'immersion attendu (Approche diégétique implicite)
	- Aucun guide pour servir de référence
- **Implémentation de la démarche :**
Sondage

Les testeurs sont divisés en 3 groupe qui vont expérimentés la même cinématiques mais avec une méthode de guidage différentes.

Pour pouvoir vraiment comprendre la cinématique, ils ont mis des petits détails très important dans les coins du point de vue. Si un élément est manqué, l'histoire peut être interprété de manière très différente.

- **Les résultats :**
Peu significatif, mais permet d'avoir des indications

Il semblerait que le blocage du mouvement de tête limiterait l'immersion. 
La luciole a été en moyenne perçu comme plus utile et immersive 

Pour expliquer les résultats peu significatif et a améliorer :
- Trop peu de candidat
- Des critères pour la mesure de l'immersion pas assez objectif
- Une scène pas assez complexe

## Articles connexes

### Article 1

<details><summary>Cliquer pour voir</summary>

#### Référence & indicateur

- **Citation :** 
Nahal Norouzi, Student Member, IEEE, Gerd Bruder, Member, IEEE, Austin Erickson, Student Member, IEEE,
Kangsoo Kim, Member, IEEE, Jeremy Bailenson, Pamela Wisniewski, Charlie Hughes, Senior Member, IEEE,
and Greg Welch, Senior Member, IEEE

Virtual Animals as Diegetic Attention Guidance Mechanisms in
360-Degree Experiences
IEEE TRANSACTIONS ON VISUALIZATION AND COMPUTER GRAPHICS, VOL. 27, NO. 11, NOVEMBER 2021

- **Conférence / Revue :**  IEEE TRANSACTIONS ON VISUALIZATION AND COMPUTER GRAPHICS, VOL. 27, NO. 11, NOVEMBER 2021
 
- **Classification :** A*

- **Nombre de citations :** 1

DOI: 10.1109/TVCG.2021.3106490

#### Résumé 

- **Problématique**
 	Guider les utilisateurs vers un endroit donné dans un monde en RV où l'on peut se mouvoir

- **Pistes possibles**
 	Utilisation de fléches ou de différents animaux que l'on doit suivre
- **Question de recherche**
 	Est ce que l'utilisation des animaux comme moyen de nous attirer vers un endroit est est efficace ?
- **Démarche adoptée**
 	Comparer différent type de guide pour savoir lequel est le plus satisfaisant en terme d'immersion et le plus apprécié globalement.
- **Implémentation de la démarche**
 	Faire une (etude interne ?) avec 5 cas différent :
	- Sans rien
	- Avec une fléche (Non diégétique)
	- Avec un oiseau
	- Avec un chien
	- Avec un chien qui sait que tu existes
- **Les résultats**
 	Le chien qui reconnait ta présence a reçu les réponses les plus positives comparé a la flèche non diégétique.
 	L'étude en a déduit des cases que les guides doivent cocher : 
	- Le fait qu'il se fonde dans l'environment
	- Le fait qu'il reconnaisse l'utilisateur
	- Le fait qu'il plait a l'utilisateur 
</details>


### Article 2

<details><summary>Cliquer pour voir</summary>

#### Référence & indicateur

- **Citation :**
 Daniel Lange, Tim Claudius Stratmann ,Susanne Boll

 HiveFive: Immersion Preserving Attention Guidance
 in Virtual Reality
- **Conférence / Revue :** 
 CHI 2020, April 25–30, 2020, Honolulu, HI, USA  
- **Classification :** 
 A*
- **Nombre de citations :** 
 12

#### Résumé 

- **Problématique :**
 Attirer l'oeil de l'utilisateur sur un endroit précis dans un monde virtuelle a 360°.
- **Pistes possibles :**
 Voir si des mouvements enregistrés sur de vrai être vivant sont plus immersifs que des mouvements généré par algo.
- **Question de recherche :**
 Comment guider l'utilisateur dans une scène sans perdre d'immertion.
- **Démarche adoptée :**
 Comparer les deux types de mouvements avec un essaim d'abeilles dans 2 environment différents (Nature et Ville).
 Comparer cet essaim d'abeilles avec d'autres types de guidance.
- **Implémentation de la démarche :**
	- 1er test : Les 2 essaims sont placés au même endroit et les utilisateurs test doivent détecter l'essaim (Création de 2 environments) :
		- Test dans la nature avec peu de mouvement. 
		- Test en Ville avec beaucoup de mouvement (Voiture qui roule)

	- 2eme test : On compare l'essaim avec d'autres techniques de pointage dans un forêt virtuelle. Une simulation de vent a été activé pour créer du mouvement dans les arbres et un pommier a été aujouté comme cible. Les technique utilisées sont :
		- Rien du tout
		- Flèche 
		- Floutage partout sauf sur le pommier
		- "DeadEye" : Faire apparaitre le pommier uniquement sur un oeil 
		- Rond qui clignote autour du pommier (Subtle Gaze Direction)
		- HiveFive (Notre essaim)
	
- **Les résultats :**
	- Pour le 1er test : Les mouvements générés par algorythmes sont bien mieux détectés que les mouvements naturels d'un essaim. Dans un environment sans mouvement comme une plaine, les techniques fonctionnent très bien. Mais en ville avec des voitures et plein de mouvements, c'est beaucoup plus difficile.
	- Pour le 2ème test, HiveFive donne de meilleurs résultats que toutes les autres techniques et également a le moins d'impact négatif sur l'immersion selon les utilisateurs.
</details>

### Article 3

<details><summary>Cliquer pour voir</summary>

#### Référence & indicateur

- **Citation :**
Subtle Attention Guidance for a New Virtual
Reality Game

Alexandre Berthault
, Camille Richard
, Mathieu Anthoine
, Florian Wolf
, Mael Addoum ¨

ISART Digital, Paris, France
- **Conférence / Revue :** 2022 IEEE Conference on Games (CoG)
- **Classification :** A*
- **Nombre de citations :** 0 

#### Résumé 

**Problématique :**
 Garder l'attention de l'utilisateur sur un objectif/ endroit précis.

**Pistes possibles**
Créer l'attention avec des effets visuels ,des éléments de gameplay ou encore la couleur de l'environment.

**Question de recherche**
Dans les jeux vidéos en VR, il est difficile de garder l'attention du joueur sur le plot de l'histoire ou le gameplay a cause de tout l'aspect interactif que la VR propose. Le but est de proposer un approche intuitive pour faciliter cette attention.

**Démarche adoptée**
Jeu d'archer où le but est de viser des cibles. On pourra donc voir différentes méthodes pour attirer le regard vers les cibles ainsi que garder l'intéret du joueur

**Implémentation de la démarche**
Tutoriel interactif pour faire comprendre au joueur le but et les mécaniques du jeu : 
- Le tutoriel évolue avec la progression du joueur plutot que d'avoir plusieurs niveaux éparpillés (Permet de garder l'attention du joueur et d'être récompensés visuellement et donc d'être plus impliqué dans le jeu)
- Des contrastes de couleur entre les objectifs et le reste du niveau pour améliorer la perception du joueur

Une interface uniquement graphique pour plus d'immersion : 
- Un petite étoile bleue qui avance vers les différents point d'interet 
- Des rayons lumineux pour diriger le regard du joueur de manière naturelle
- Pour tirer des fléches, il y a l'intérieur de l'arc une jauge pour connaitre la puissance du tir ainsi qu'un rayon pour prédire la trajectoire de la flèche (Le rayon devient vert pour indiquer que la flèche est sur une cible)


**Les résultats**
16 personnes ont participés a l'expérience : (Quelques stats)
- 90% ont compris directement les mécaniques du jeu
- 80% ont senti que quelques choses guider leur regard (Objectif réussi) 
- 66% sont content de terminé un niveau (On a donc un retour satistaifant au système de progression)

Dans l'ensemble, leur règles ont bien été comprises grace au système intuitif des cibles ainsi que le rayon pour connaitre la trajectoire de la flèche
Selon les utilisateurs, la trajectoire des flèches est très appréciés et rends l'expérience beaucoup plus accessible et garde l'expérience satisfaisante et fluide.

Le fait de guider subtilement le regard de l'utilisateur permet de rendre plus satisfaisant la réussite d'un niveau dû a un sens d'autonomie plus grand
</details>

### Article 4

<details><summary>Cliquer pour voir</summary>

#### Référence & indicateur

- **Citation :** 
Chong Cao ,Zhaowei ,Shi Miao Yu
Automatic Generation of Diegetic Guidance in Cinematic Virtual Reality
2020 IEEE International Symposium on Mixed and Augmented Reality (ISMAR)
- **Conférence / Revue :**
IEEE
- **Classification de la conférence / Revue :**
A*
- **Nombre de citations :**
2 

#### Résumé 

- **Problématique  :**
L'utilisateur pourrait manquer des éléments visuels important s'il n'est pas orienté du bon coté au bon moment dans des Cinématiques en RV
- Pistes possibles (pointés par les auteurs) :
	- Mettre une grosse flèche sur l'écran (Pas très subtil)
	- Assombrir l'image (Pour indiquer que l'on sort du champs)
	- Génerer automatiquement un élément du monde pour attirer l'attention de l'utilisateur (But du papier)
- **Question de recherche :**
Savoir si une guide diégétique géneré automatiquement est plus efficace et naturelle pour guider l'utilisateur que d'autres solutions diégétique manuelle déjà utilisé dans le contexte de CVR (Cinematic Virtual Reality)
- **Démarche adoptée :**
Comparer la rapidité, le ressenti utilisateur et l'efficacité des deux méthodes (Auto et manuelle) avec une animation 
- **Implémentation de la démarche :**
Création d'une petite animation (Dragon) testé par 70 participant.
	- 1er Test d'orientation de l'utilisateur avec des flèches dans le monde (Défini manuellement,non diégétique)
	- 2eme test avec un corbeau qui vole en direction de l'événement (Défini automatique, diégétique)
	- 3eme test, pas de guidance


- **Les résultats :**
Les guides diégétiques obtiennent de meilleurs résultats dans l'ensemble que les guides non diégétiques. 
Néanmoins en efficacité, toutes formes de guide donne un résultat de 100% au test de suivi du scénario, pour seulement 40% sans guide.
Les guides diégétiques apportent plus d'immersion.

</details>

## Grille d'analyse

|  | Type d'interface | Type d'experience | Action possible | Mouvement possible | But |
| ------ | ------ | ------ | ------ | ------ | ------ |
| Article de Ref | Diégétique/Non Diégétique | Cinematic VR |  Aucun | Mouvement de tête et déplacement limité | Mesurer l'efficacité d'une méthode diégétique et une non-diégétique de guidage pour suivre un scénario |
| Art 1 : Diégétique Animal | Spatial/Diégétique | Monde Libre | Aucune | Mouvement de tête | Rapidité de détection et Immersion  |
| Art 2 : HiveFive |  Mixte | Monde Libre | Suivre/Marcher | Libre | Immersion et satisfabilité |
| Art 3 : SAG VR Game | Diégétique/Spatial | Jeu | Tirer des flèches | Mouvement de tête | Satisfaction et efficacité de la détection |
| Art 4 : Cinematic diegetic | Diégétique/Spatial| Cinematic VR | Aucune | Mouvement de tête | Diriger l'attention et Immersion |

## Question de recherche

Mon travail de recherche vise a étudié l'impact des interfaces diégétiques dans une expérience Cinematic VR pour guider l'attention des utilisateurs vers les détails important d'une scène.
Les méthodes qui seront testés seront : 
- Deux méthodes diégétiques : 
	- Des lucioles (Petites flammes) (Diégétique implicites)
	- Une souris blanche (Diégétique explicite)
- 3 méthodes non diégétiques :
	- Une flèche (Non diégétique explicité)
	- Floutage sauf sur la scène ou l'objet (Non diégétique implicite)
	- Subtle Gaze Direction (SGD -> clignotement subtil)
- Aucun guidage

Le but sera de déterminé l'efficacité de chaque méthode a diriger l'utilisateur vers les éléments scénaristique important ainsi que les petits détails, mais aussi l'impact que ces méthodes ont sur la sensation d'immersion.

## Plan de la démarche adoptée

Pour notre question de recherche, nous allons effectuer un sondage suite au visionnage d'une cinématique.
Chaques utilisateurs vont voir la même expérience mais avec un système de guidage différents.

Cela nous permettra de déterminer le nombre d'élément important détecté, le niveau de satisfaction de l'utilisateur sur la fluidité, l'immersion et l'impression d'autonomie.

Le protocole est le suivant : 
Une cinématique dans un manoir hanté, assez sombre avec des éléments scénaristique plus ou moins fondu dans l'obscurité.
Les méthodes utilisés ont des couleurs ou une luminance assez importante pour contraster et être plus facile a voir.
La cinématique consiste a être attaché sur une chaise, assis , pendant que tout un tas d'événement paranormal vont se passer (Des fantomes, une lampe qui bouge, un libre qui écrit)
L'utilisateur pourra uniquement bouger la tête et se retourner et il devra au mieux suivre le scénario. 
